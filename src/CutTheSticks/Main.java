package CutTheSticks;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
public class Main {
	public static void main(String[] args) {
		List<Integer> input = new ArrayList<Integer>();
		input.add(1);
		input.add(2);
		input.add(3);
		input.add(4);
		input.add(3);
		input.add(3);
		input.add(2);
		input.add(1);
		List<Integer> result = new ArrayList<Integer>();
		cutTheSticks(input, result, input.size());
		displayList(result);
	}
	public static List<Integer> cutTheSticks(List<Integer> input,List<Integer> result, int size){
		if(size != 0) {
			result.add(size);
			int small = input.get(0);
			for(int i = 0;i<size;i++) {
				if(input.get(i) < small) {
					small = input.get(i);
				}
			} 
			for(int j = 0;j<size;j++) {
				input.set(j, input.get(j)-small);
				
			}
			Iterator<Integer> iterator = input.iterator();
			while(iterator.hasNext()) {
				int element = (int) iterator.next();
				if(element == 0) {
					iterator.remove();
				}	
			}
		}
		if(size == 0) {
			return input;
		}
		else {
			return cutTheSticks(input, result, input.size());
		}	
	}
	public static void displayList(List<Integer> list) {
		int length = list.size();
		for(int i = 0;i<length;i++) {
			if(i == length-1) {
				System.out.print(list.get(i));
			}
			else {
				System.out.print(list.get(i) + ",");
			}
		}
	}
}
